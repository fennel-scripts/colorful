#!/bin/env fennel
;; --add-fennel-path $XDG_CONFIG_HOME/colourful/?.fnl
(local theme (require :theme))
(local colourful {})


;;; code:
(fn getColour [key]
  (. theme key)
  )


colourful
