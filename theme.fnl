#!/bin/env fennel
;; -*- mode: rainbow; -*-
(local theme {})


;;; code:

(local mycolors
             {
                :colour0 "#232635"
                :colour1 "#292d3e"
                :colour2 "#3c435e"
                :colour3 "#c792ea"
                :colour4 "#adcd81"
                :colour5 "#ffcb6b"
                :colour6 "#697098"
                :colour7 "#717cb4"
                :colour8 "#963c59"
                :colour9 "#43827b"
                }
             )

;;;; theme
(local myTheme
       {
        :bg             mycolors.colour1
        :fg             mycolors.colour5
        :background     mycolors.colour1
        :foreground     mycolors.colour5
        :text           mycolors.colour5
        :pink           mycolors.colour3
        :green          mycolors.colour4
        :matteGreen     mycolors.colour9
        :yellow         mycolors.colour5
        :red            mycolors.colour8
        }
       )



theme
